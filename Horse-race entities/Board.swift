//
//  Board.swift
//  Horse-race entities
//
//  Created by Bruno Barbosa on 3/30/16.
//  Copyright © 2016 GDR. All rights reserved.
//

import UIKit
import Foundation
import CoreData

@objc(Board)
class Board: NSManagedObject {
    
    func setSteps(newBoard: Board, members: NSArray, managedContext: NSManagedObjectContext)
    {
        var newSteps = [Step]()
        
        for index in 0 ..< 6
        {
            let entity = NSEntityDescription.entityForName("Step", inManagedObjectContext: managedContext)
            
            let step = Step(entity: entity!, insertIntoManagedObjectContext: managedContext)
            
            
            step.value = ""
            step.done = false
            
            switch index
            {
            case 0:
                step.name = "Big Idea"
                step.icon = "big-idea"
                break
            case 1:
                step.name = "Essential Question"
                step.icon = "essential-question"
                break
            case 2:
                step.name = "Challenge"
                step.icon = "challenge"
                break
            case 3:
                step.name = "Solution"
                step.icon = "solution"
                break
            case 4:
                step.name = "Implementation"
                step.icon = "implementation"
                break
            case 5:
                step.name = "Evaluation"
                step.icon = "evaluation"
                break
            default:
                print("deu ruim")
                break
            }
            
            step.board = newBoard
            
            newSteps.append(step)
            
            print(newSteps.last?.name)
        }
        
        do
        {
            newBoard.steps = NSOrderedSet.init(array: newSteps as [AnyObject])
            
            try managedContext.save()
            
        } catch let error as NSError {
            print("Could not save \(error), \(error.userInfo)")
        }
    }

    
    func completeStep(value value: String, index: Int, managedContext: NSManagedObjectContext)
    {
        let step = self.steps?.objectAtIndex(index) as! Step
        
        do
        {
            step.value = value
            step.done = true
            
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    func isOnTestflight()
    {
        self.testFlight = true
    }
    
    func isOnAppStore()
    {
        self.appStore = true
    }
    
    func resetStep(index: Int, managedContext: NSManagedObjectContext)
    {
        let step = self.steps?.objectAtIndex(index) as! Step
        
        do
        {
            step.value = ""
            step.done = false
            
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save \(error), \(error.userInfo)")
        }
    }

}
