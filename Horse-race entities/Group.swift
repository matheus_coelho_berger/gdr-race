//
//  Group.swift
//  Horse-race entities
//
//  Created by Matheus Coelho Berger on 3/22/16.
//  Copyright © 2016 GDR. All rights reserved.
//

import Foundation
import CoreData


class Group: NSManagedObject {

// Insert code here to add functionality to your managed object subclass
    
    func newGroup(members: NSArray, project: Board, managedContext: NSManagedObjectContext)
    {
        var newMembers = [Student]()
        
        for index in 0 ..< members.count
        {
            let member = self.registerMember((members.objectAtIndex(index) as? String)!, project: project, managedContext: managedContext)
            
            newMembers.append(member)
        }
        
        self.members = NSOrderedSet.init(array: newMembers)
    }
    
    override func awakeFromInsert()
    {
        super.awakeFromInsert()
        
        if self.members == nil
        {
            let student1 = Student()
            student1.name = "Batman"
            let student2 = Student()
            student2.name = "Superman"
            
            let newSet = NSOrderedSet(array: [student2, student1])
            self.members = newSet
        }
    }
    
    func registerMember(name: String, project: Board, managedContext: NSManagedObjectContext) -> Student
    {
        let entity = NSEntityDescription.entityForName("Student", inManagedObjectContext: managedContext)
        
        let student = Student(entity: entity!, insertIntoManagedObjectContext: managedContext)
        
        do
        {
            student.name = name
            student.registerProject(project)
            
            try managedContext.save()
            
        } catch let error as NSError {
            print("Could not save \(error), \(error.userInfo)")
        }
        
        return student
    }
}
