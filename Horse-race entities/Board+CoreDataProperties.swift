//
//  Board+CoreDataProperties.swift
//  Horse-race entities
//
//  Created by Matheus Coelho Berger on 5/25/16.
//  Copyright © 2016 GDR. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Board {

    @NSManaged var nameGroup: String?
    @NSManaged var testFlight: Bool
    @NSManaged var appStore: Bool
    @NSManaged var steps: NSOrderedSet?

}
