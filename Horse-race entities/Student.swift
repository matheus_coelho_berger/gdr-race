//
//  Student.swift
//  Horse-race entities
//
//  Created by Matheus Coelho Berger on 3/22/16.
//  Copyright © 2016 GDR. All rights reserved.
//

import Foundation
import CoreData


class Student: NSManagedObject {

// Insert code here to add functionality to your managed object subclass
    
    func registerProject(project: Board)
    {
        var temp = self.projects?.array
        
        temp?.append(project)
        self.projects = NSOrderedSet(array: temp!)
    }
    
}
