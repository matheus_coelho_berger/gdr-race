//
//  PopoverViewController.swift
//  Horse-race entities
//
//  Created by Bruno Barbosa on 3/29/16.
//  Copyright © 2016 GDR. All rights reserved.
//

import UIKit

protocol PopOverDelegate {
    func didDeleteBoard()
}

class PopoverViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var stepsCollection: UICollectionView!
    @IBOutlet weak var descriptionCard: UIView!
    @IBOutlet weak var groupName: UILabel!
    @IBOutlet var groupCard: UIView!
    
    var partida = Partida()
    var board: Board!
    var delegate: PopOverDelegate!
    
    //var members = ["Bruno Barbosa", "Renata Motta", "Matheus Bolsoni", "Vinicius Santana", "Luiz Reis"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.partida.resumirPartida()
        self.configureDescriptionCard()
        self.configEditGroupName()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configureDescriptionCard() {
        self.descriptionCard.backgroundColor = UIColor.whiteColor()
        self.descriptionCard.layer.cornerRadius = 10
        self.descriptionCard.layer.borderColor = UIColor(red: 198/255.0, green: 203/255.0, blue: 216/255.0, alpha: 1.0).CGColor
        self.descriptionCard.layer.borderWidth = 2.0
        
        self.groupName.text = board.nameGroup
    }
    
    func configEditGroupName() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(PopoverViewController.editGroupName))
        self.groupCard.addGestureRecognizer(tap)
        
    }
    
    func editGroupName() {
        let alert = UIAlertController(title: "Editar Grupo", message: "Qual o nome do grupo?", preferredStyle: UIAlertControllerStyle.Alert)
         
        alert.addTextFieldWithConfigurationHandler({ (textField) -> Void in
            textField.text = self.board.nameGroup
         })
         
         alert.addAction(UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.Cancel, handler: nil))
         
         alert.addAction(UIAlertAction(title: "Salvar", style: .Default, handler: { (action) -> Void in
            let textField = alert.textFields![0] as UITextField
            self.board.nameGroup = textField.text
            self.groupName.text = textField.text
         }))
         
         self.presentViewController(alert, animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
  /*
    // MARK: - TableView
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("MemberCell")! as! MemberTableViewCell
        //cell.memberName.text = self.members[indexPath.row]
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 25
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
*/
    
    // MARK: - CollectionView
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return board.steps!.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("CardCell", forIndexPath: indexPath) as! StepCollectionViewCell
        cell.step = self.board.steps![indexPath.row] as? Step
        cell.layer.borderColor = UIColor(red: 205/255.0, green: 205/255.0, blue: 205/255.0, alpha: 1.0).CGColor
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let step = board.steps![indexPath.row] as! Step
        let alert = UIAlertController(title: step.name, message: "Digite o novo valor", preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addTextFieldWithConfigurationHandler({ (textField) -> Void in
            if step.done {
                textField.text = step.value
            }
            
        })
        if step.done {
            alert.addAction(UIAlertAction(title: "Resetar", style: UIAlertActionStyle.Destructive, handler: { (action) -> Void in
                self.partida.resetStepFromBoard(self.board, step: indexPath.row)
                collectionView.reloadData()
            }))
        }
        
        alert.addAction(UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.Cancel, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Adicionar", style: .Default, handler: { (action) -> Void in
            let textField = alert.textFields![0] as UITextField
            self.partida.completeStepFromGroup(self.board, value: textField.text!, step: indexPath.row)
            collectionView.reloadData()
        }))
        
        self.presentViewController(alert, animated: true, completion: nil)
    }

    @IBAction func deleteBoard(sender: AnyObject)
    {
        let alert = UIAlertController(title: "Deletar grupo", message: "Você tem certeza que deseja deletar esse grupo?", preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.Cancel, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Deletar", style: .Destructive, handler: { (action) -> Void in
            self.partida.deleteBoard(board: self.board!)
            self.dismissViewControllerAnimated(true) {
                self.delegate.didDeleteBoard()
            }
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
}
