//
//  Etapa+CoreDataProperties.swift
//  Horse-race entities
//
//  Created by Bruno Barbosa on 3/30/16.
//  Copyright © 2016 GDR. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Etapa {

    @NSManaged var concluida: Bool
    @NSManaged var icone: String?
    @NSManaged var nome: String?
    @NSManaged var resposta: String?
    @NSManaged var tabuleiro: Tabuleiro?

}
