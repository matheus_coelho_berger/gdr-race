//
//  MemberTableViewCell.swift
//  Horse-race entities
//
//  Created by Bruno Barbosa on 3/29/16.
//  Copyright © 2016 GDR. All rights reserved.
//

import UIKit

class MemberTableViewCell: UITableViewCell {

    @IBOutlet weak var memberName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
