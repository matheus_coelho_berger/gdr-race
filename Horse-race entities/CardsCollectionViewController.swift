//
//  CardsCollectionViewController.swift
//  Horse-race entities
//
//  Created by Bruno Barbosa on 3/20/16.
//  Copyright © 2016 GDR. All rights reserved.
//

import UIKit

class CardsCollectionViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UIPopoverPresentationControllerDelegate, PopOverDelegate  {

    @IBOutlet weak var segueInvisibleAnchor: UIView!
    @IBOutlet weak var cardsCollection: UICollectionView!
    
    var partida = Partida()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.partida.resumirPartida()
        self.cardsCollection.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Interactions
    func blurBackground() -> UIView {
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Dark)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.frame = self.view.frame
        blurView.alpha = 0
        self.view.addSubview(blurView)
        UIView.animateWithDuration(0.25) { () -> Void in
            blurView.alpha = 1.0
        }
        return blurView
    }
    
    func highlightSelectedCard(frame frame: CGRect, group: Board) -> UIView {
        let highlightedCard = UIView(frame: frame)
        highlightedCard.layer.borderColor = UIColor(red: 198/255.0, green: 203/255.0, blue: 216/255.0, alpha: 1.0).CGColor
        highlightedCard.layer.borderWidth = 2.0
        highlightedCard.layer.cornerRadius = 10
        highlightedCard.backgroundColor = UIColor.whiteColor()
        
        return highlightedCard
    }

    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        if segue.identifier == "GroupDownArrowSegue" || segue.identifier == "GroupUpArrowSegue" {
            let vc = segue.destinationViewController as! PopoverViewController
            vc.popoverPresentationController!.delegate = self
            vc.board = sender as! Board
            vc.delegate = self
        }
    }

    // MARK: UICollectionViewDataSource

    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }


    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == -1 {
            print(partida.boards.count)
            return partida.boards.count + 1
        } else {
            return partida.boards[collectionView.tag].steps!.count
        }
    }

    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        if collectionView.tag == -1 {
            if indexPath.row == partida.boards.count {
                let cell = collectionView.dequeueReusableCellWithReuseIdentifier("AddNewCell", forIndexPath: indexPath)
                cell.layer.borderColor = UIColor(red: 198/255.0, green: 203/255.0, blue: 216/255.0, alpha: 1.0).CGColor
                return cell
            } else {
                let cell = collectionView.dequeueReusableCellWithReuseIdentifier("CardCell", forIndexPath: indexPath) as! CardCollectionViewCell
                cell.groupName.text = partida.boards[indexPath.row].nameGroup
                
                cell.layer.borderColor = UIColor(red: 198/255.0, green: 203/255.0, blue: 216/255.0, alpha: 1.0).CGColor
                return cell
            }
        } else {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("IconCell", forIndexPath: indexPath) as! IconCardCollectionViewCell
//            cell.backgroundColor = self.groups[collectionView.tag][indexPath.item]
            let step = partida.boards[collectionView.tag].steps![indexPath.row] as! Step
            var iconName = step.icon!
            if !step.done {
                iconName += "-null"
            }
            iconName += ".png"
            cell.icon.image = UIImage(named: iconName)
            return cell
        }
    }

    // MARK: UICollectionViewDelegate

    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if collectionView.tag == -1 {
            if indexPath.row == partida.boards.count {
                let alert = UIAlertController(title: "Adicionar Grupo", message: "Qual o nome do grupo?", preferredStyle: UIAlertControllerStyle.Alert)
                
                alert.addTextFieldWithConfigurationHandler({ (textField) -> Void in
                    textField.text = "Grupo \(self.partida.boards.count+1)"
                    
                })
                
                alert.addAction(UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.Cancel, handler: nil))
                
                alert.addAction(UIAlertAction(title: "Adicionar", style: .Default, handler: { (action) -> Void in
                    let textField = alert.textFields![0] as UITextField
                    self.partida.newBoard(textField.text!, members: ["Bruno", "Mateus", "Matheus", "Vinicius"])
                    collectionView.reloadData()
                }))
                
                self.presentViewController(alert, animated: true, completion: nil)
            } else {
                let selectedCell = collectionView.cellForItemAtIndexPath(indexPath)
                var segueName = "GroupUpArrowSegue"
                let xPos = selectedCell!.frame.origin.x + selectedCell!.frame.size.width/2 + collectionView.frame.origin.x
                var yPos = selectedCell!.frame.origin.y + selectedCell!.frame.size.height + collectionView.frame.origin.y
                if indexPath.row > 4 {
                    segueName = "GroupDownArrowSegue"
                    yPos = collectionView.frame.origin.y + collectionView.frame.size.height - selectedCell!.frame.size.height
                }
                
                segueInvisibleAnchor.frame = CGRect(x: xPos, y: yPos, width: 0, height: 0)
                self.performSegueWithIdentifier(segueName, sender: self.partida.boards[indexPath.row])
            }
        }
        /*else {
            let etapa = partida.tabuleiros[collectionView.tag].etapas![indexPath.row] as! Etapa
            let alert = UIAlertController(title: etapa.nome, message: "Digite o novo valor", preferredStyle: UIAlertControllerStyle.Alert)
            
            alert.addTextFieldWithConfigurationHandler({ (textField) -> Void in
                textField.text = etapa.resposta
                
            })
            if etapa.concluida {
                alert.addAction(UIAlertAction(title: "Resetar", style: UIAlertActionStyle.Destructive, handler: { (action) -> Void in
                    self.partida.resetarEtapa(collectionView.tag, etapa: indexPath.row)
                    collectionView.reloadData()
                }))
            }
            
            alert.addAction(UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.Cancel, handler: nil))
            
            alert.addAction(UIAlertAction(title: "Adicionar", style: .Default, handler: { (action) -> Void in
                let textField = alert.textFields![0] as UITextField
                self.partida.completarEtapa(collectionView.tag, resposta: textField.text!, etapa: indexPath.row)
                collectionView.reloadData()
            }))
            
            self.presentViewController(alert, animated: true, completion: nil)
        }*/
    }
    
    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        
        guard let collectionViewCell = cell as? CardCollectionViewCell else {return}
        
        collectionViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
    }
    
    
    // MARK: - Popover
    
    func prepareForPopoverPresentation(popoverPresentationController: UIPopoverPresentationController) {
//        self.blurBackground()
    }
    
    func popoverPresentationControllerDidDismissPopover(popoverPresentationController: UIPopoverPresentationController) {
        self.cardsCollection.reloadData()
    }
    
    func didDeleteBoard() {
        self.partida.resumirPartida()
        self.cardsCollection.reloadData()
    }

}
