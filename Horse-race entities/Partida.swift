//
//  Partida.swift
//  Horse-race entities
//
//  Created by Matheus Coelho Berger on 3/19/16.
//  Copyright © 2016 GDR. All rights reserved.
//


/// =============================== COMO USAR ESSA DESGRAÇA =======================================///
/// sempre chamar a função resumirPartida no viewWillAppear do viewController inicial para realizar
/// o fetch de todos os grupos

/// Pra registrar um novo grupo é só chamar a função novoTabuleiro e passar o nome do grupo

/// Para usar a função completarEtapa basta passar o index do grupo desejado, junto com o index da 
/// etapa (0 = big idea, 1 = essential question, ... , 5 = evaluation), e a string com a resposta.

/// Pra resetar uma etapa basta passar o index do grupo e o index da etapa no mesmo modelo do de completar

import UIKit
import CoreData

class Partida: NSObject
{
    var boards = [Board]()
    
    func resumirPartida()
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        
        let fetchRequest = NSFetchRequest(entityName: "Board")
        
        do {
            let results = try managedContext.executeFetchRequest(fetchRequest)
            
            boards = results as! [Board]
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }

    }
    
    func completeStep(board: Int, answer: String, step: Int)
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        self.boards[board].completeStep(value: answer, index: step, managedContext: managedContext)
    }
    
    func completeStepFromGroup(board: Board, value: String, step: Int)
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        board.completeStep(value: value, index: step, managedContext: managedContext)
        
    }
    
    func resetStep(board: Int, step: Int)
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        self.boards[board].resetStep(step, managedContext: managedContext)
    }
    
    func resetStepFromBoard(board: Board, step: Int) {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        board.resetStep(step, managedContext: managedContext)
    }
    
    func newBoard(name: String, members: [String])
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        
        
        let entity = NSEntityDescription.entityForName("Board", inManagedObjectContext: managedContext)
        let newBoard = Board(entity: entity!, insertIntoManagedObjectContext: managedContext)

        
        newBoard.setValue(name, forKey: "nameGroup")
        
        newBoard.setSteps(newBoard, members: ["Bruno", "Matheus", "Mateus", "Vinicius"], managedContext: managedContext)
        
        do
        {
            try managedContext.save()
            boards.append(newBoard)
        } catch let error as NSError {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    func deleteBoard(board deletedBoard: Board)
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        
        let index = self.boards.indexOf(deletedBoard)
        print(self.boards[index!])
        
        self.boards.removeAtIndex(index!)
//        print(self.boards[index!])
        
        managedContext.deleteObject(deletedBoard as NSManagedObject)
        
        do
        {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
}
