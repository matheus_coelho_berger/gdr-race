//
//  StepCollectionViewCell.swift
//  Horse-race entities
//
//  Created by Bruno Barbosa on 3/29/16.
//  Copyright © 2016 GDR. All rights reserved.
//

import UIKit

enum CardColors {
    case yellow
    case blue
    case green
    
    static func getColor(step: Step) -> UIColor {
        switch step.icon! {
        case "big-idea", "essential-question", "challenge":
            // (235,204,48)
            return UIColor(red: 235/255.0, green: 204/255.0, blue: 48/255.0, alpha: 1.0)
        case "solution":
            // (112,166,219)
            return UIColor(red: 112/255.0, green: 166/255.0, blue: 219/255.0, alpha: 1.0)
        case "implementation", "evaluation":
            // (119,178,136)
            return UIColor(red: 119/255.0, green: 178/255.0, blue: 136/255.0, alpha: 1.0)
        default:
            return UIColor(red: 235/255.0, green: 204/255.0, blue: 48/255.0, alpha: 1.0)
        }
    }
    
    func getColor() -> UIColor {
        switch self {
        case .yellow:
            return UIColor(red: 235/255.0, green: 204/255.0, blue: 48/255.0, alpha: 1.0)
        case .blue:
            return UIColor(red: 112/255.0, green: 166/255.0, blue: 219/255.0, alpha: 1.0)
        case .green:
            return UIColor(red: 119/255.0, green: 178/255.0, blue: 136/255.0, alpha: 1.0)
        }
    }
}

class StepCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblStepName: UILabel!
    @IBOutlet weak var lblStepValue: UITextView!
    @IBOutlet weak var imgStep: UIImageView!
    
    var stepName: String! = "" {
        didSet {
            lblStepName.text = self.stepName
        }
    }
    
    var stepValue: String! = "" {
        didSet {
            lblStepValue.text = self.stepValue
        }
    }
    
    var step: Step? {
        didSet {
            self.stepName = self.step!.name!
            self.stepValue = self.step!.value!
            self.imgStep.image = UIImage(named: "\(self.step!.icon!)-card.png")
            if self.step!.done {
                self.backgroundColor = CardColors.getColor(self.step!)
            }
        }
    }
}
