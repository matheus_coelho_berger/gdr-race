//
//  Step+CoreDataProperties.swift
//  Horse-race entities
//
//  Created by Matheus Coelho Berger on 5/25/16.
//  Copyright © 2016 GDR. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Step {

    @NSManaged var done: Bool
    @NSManaged var icon: String?
    @NSManaged var name: String?
    @NSManaged var value: String?
    @NSManaged var board: Board?

}
