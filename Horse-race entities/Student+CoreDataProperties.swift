//
//  Student+CoreDataProperties.swift
//  Horse-race entities
//
//  Created by Bruno Barbosa on 3/30/16.
//  Copyright © 2016 GDR. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Student {

    @NSManaged var name: String?
    @NSManaged var projects: NSOrderedSet?

}
