//
//  CardCollectionViewCell.swift
//  Horse-race entities
//
//  Created by Bruno Barbosa on 3/20/16.
//  Copyright © 2016 GDR. All rights reserved.
//

import UIKit

class CardCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var groupName: UILabel!
    @IBOutlet private weak var etapasCollection: UICollectionView!
    
    func setCollectionViewDataSourceDelegate
        <D: protocol<UICollectionViewDataSource, UICollectionViewDelegate>>
        (dataSourceDelegate: D, forRow row: Int) {
            
            etapasCollection.delegate = dataSourceDelegate
            etapasCollection.dataSource = dataSourceDelegate
            etapasCollection.tag = row
            etapasCollection.reloadData()
    }
}
