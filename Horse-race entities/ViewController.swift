//
//  ViewController.swift
//  Horse-race entities
//
//  Created by Matheus Coelho Berger on 3/19/16.
//  Copyright © 2016 GDR. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource {

    var partida = Partida()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.partida.resumirPartida()
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func checarGrupo(grupo: Int)
    {
        print(self.partida.boards[0])
    }
    
    @IBAction func completar(sender: AnyObject)
    {
        let step = self.partida.boards[0].steps?.objectAtIndex(0) as! Step
        
        print(step.done, step.done)
        
        self.partida.completeStep(0, answer: "big idea padrao", step: 0)
        
        print(step.value, step.done)
    }

    @IBAction func resetar(sender: AnyObject)
    {
        let step = self.partida.boards[0].steps?.objectAtIndex(0) as! Step
        
        print(step.value, step.done)
        
        self.partida.resetStep(0, step: 0)
        
        print(step.value, step.done)
    }
   
    @IBAction func addGrupo(sender: AnyObject)
    {
        //partida.newBoard(textField.text!)
        self.checarGrupo(0)
        self.tableView.reloadData()
        
        self.textField.resignFirstResponder()
    }
    
    //tableview
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return partida.boards.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell")
        
        let tabuleiro = partida.boards[indexPath.row]
        
        cell!.textLabel!.text = tabuleiro.valueForKey("nomeGrupo") as? String
        
        return cell!
    }
}

