//
//  IconCardCollectionViewCell.swift
//  Horse-race entities
//
//  Created by Bruno Barbosa on 3/20/16.
//  Copyright © 2016 GDR. All rights reserved.
//

import UIKit

class IconCardCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var icon: UIImageView!
    
    var step: Step {
        get{
            return self.step
        }set(newStep) {
            self.step = newStep
//            if etapa.description == "" || etapa.description == nil {
//                icon.image = "big-idea-null"
//            } else {
//                icon.image = etapa.icone
//            }
        }
    }
    
}
